<?php

/**
 * @param $link
 * @param $opt
 * @return mixed
 */
function queryData($link, $opt)
{

    $ch = curl_init();
    curl_setopt_array($ch, [
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_URL => $link,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $opt
    ]);

    $exec = curl_exec($ch);

    // Если запрос не выполнен
    if (!$exec) {
        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }

    curl_close($ch);

    return $exec;
}

/**
 * @param $item
 * @return string
 */
function prepreOutput($item, $key)
{
    $outText = '';
    for ($i = 0; $i < strlen($item);) {
        for ($j = 0; ($j < strlen($key) && $i < strlen($item)); $j++, $i++) {
            $outText .= $item[$i] ^ $key[$j];
        }
    }
    return base64_encode($outText);
}

/**
 * @param $link
 * @param array $options
 * @return string
 */
function cURL_query($link, $options = ['method' => 'get'])
{
    $data = json_decode(queryData($link, $options), true);

    if (is_array($data['response'])) {
        $msg = $data['response']['message'];
        $key = $data['response']['key'];

        // возвращаем зашифрованное сообщение
        return prepreOutput($msg, $key);
    }

    return $data;

}